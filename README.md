# morele

## Getting started

clone this project

### CMD
##### composer install
##### symfony serve
### Endpoints
- [api/recommendations/random](https://127.0.0.1:8000/api/recommendations/random)
- [api/recommendations/w](https://127.0.0.1:8000/api/recommendations/w)
- [api/recommendations/multi](https://127.0.0.1:8000/api/recommendations/multi)

### Test's
##### php bin/phpunit
