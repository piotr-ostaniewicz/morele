<?php

namespace App\Services;

class RecommendationService
{
    public function filterMoviesWithW(array $movies): array
    {
        $filteredMovies = [];

        foreach ($movies as $movie) {
            if (stripos($movie, 'W') === 0 && strlen($movie) % 2 === 0) {
                $filteredMovies[] = $movie;
            }
        }

        return $filteredMovies;
    }

    public function filterMoviesWithMultiple(array $movies): array
    {
        $filteredMovies = [];

        foreach ($movies as $movie) {
            $words = preg_split('/\s+/', $movie);
            if (count($words) > 1) {
                $filteredMovies[] = $movie;
            }
        }

        return $filteredMovies;
    }
}
