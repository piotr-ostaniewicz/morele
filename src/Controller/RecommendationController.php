<?php

namespace App\Controller;

use App\Services\RecommendationService;
use App\Services\Tools\ToolsService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('api/recommendations', 'recommendations_')]
class RecommendationController extends AbstractController
{
    public function __construct(
        private readonly RecommendationService $recommendationService,
        private readonly ToolsService $toolsService
    ) {
    }

    #[Route('/random', name: 'random', methods: ['GET'])]
    public function random(): JsonResponse
    {
        return new JsonResponse(array_rand(array_flip($this->toolsService->movies()), 3), Response::HTTP_OK);
    }

    #[Route('/w', name: 'w', methods: ['GET'])]
    public function w(): JsonResponse
    {
        $filteredMovies = $this->recommendationService->filterMoviesWithW($this->toolsService->movies());
        return new JsonResponse(array_values($filteredMovies), Response::HTTP_OK);
    }

    #[Route('/multi', name: 'multi', methods: ['GET'])]
    public function multi(): JsonResponse
    {
        $filteredMovies = $this->recommendationService->filterMoviesWithMultiple($this->toolsService->movies());
        return new JsonResponse(array_values($filteredMovies), Response::HTTP_OK);
    }
}
